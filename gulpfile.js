const gulp = require('gulp');
const sass = require('gulp-sass');
const csso = require('gulp-csso');
const sourcemaps = require('gulp-sourcemaps');
const concat = require('gulp-concat');
const rename = require('gulp-rename');
const ts = require('gulp-typescript');
const uglify = require('gulp-uglify');
const pump = require('pump');
const nunjucksRender = require('gulp-nunjucks-render');
const imageResize = require('gulp-image-resize');
const data = require('gulp-data');

gulp.task('default', ['copy-assets', 'csso', 'uglify', 'nunjucks', 'resize-screenshots-all']);


//SASS
gulp.task('sass', () => {
    return gulp.src('./sass/*.scss')
        .pipe(sourcemaps.init())
            .pipe(sass().on('error', sass.logError))
            // .pipe(rename({basename: "style"}))
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest('web/css'));
});

gulp.task('dev-sass', ['sass'], () => {
    pump([
        gulp.src(['web/css/*.css', '!web/css/*.min.css']),
        rename({
            suffix: '.min'
        }),
        gulp.dest('web/css')
    ])
});

gulp.task('csso', ['sass'], () => {
    return gulp.src(['web/css/*.css', '!web/css/*.min.css'])
        .pipe(sourcemaps.init())
        .pipe(csso())
        .pipe(rename({
            suffix: ".min"
        }))
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest('web/css'));
});

//TypeScript
gulp.task('ts', () => {
    return gulp.src('./ts/*.ts')
        .pipe(sourcemaps.init())
            .pipe(ts({
                noImplicitAny: true,
                outFile: 'script.js'
            }))
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest('web/js'));
});

gulp.task('dev-ts', ['ts'], () => {
    pump([
        gulp.src(['web/js/script.js']),
        rename({
            suffix: '.min'
        }),
        gulp.dest('web/js')
    ])
});

gulp.task('uglify', ['ts'], () => {
    pump([
        gulp.src(['web/js/script.js']),
        sourcemaps.init(),
        uglify({mangle: { toplevel: true }}),
            rename({
                suffix: '.min'
            }),
        sourcemaps.write('./'),
        gulp.dest('web/js')
    ]);
});

//Nunjucks AKA Twig
gulp.task('nunjucks', () => {
    return gulp.src('app/pages/**/*.+(html|njk)')
        .pipe(data(function() {
            return require('./app/data.json')
        }))
        .pipe(nunjucksRender({
            path: ['app/templates']
        }))
        .pipe(gulp.dest('web'));
});


gulp.task('copy-assets', () => {
    return gulp.src('assets/**/**')
        .pipe(gulp.dest('web'));
});


gulp.task('resize-screenshots-all', ['copy-assets'], () => {
    resizeScreenshots(1024);
    resizeScreenshots(512);
    resizeScreenshots(256);
});


//Screenshots resize
gulp.task('resize-screenshots-1024', ['copy-assets'], () => resizeScreenshots(1024));

gulp.task('resize-screenshots-512', ['copy-assets'], () => resizeScreenshots(512));

gulp.task('resize-screenshots-256', ['copy-assets'], () => resizeScreenshots(256));

const resizeScreenshots = size => {
    return gulp.src('web/img/screenshots/orig/*.+(png|jpg)')
        .pipe(imageResize({
            width : size,
            height : size,
            crop : false,
            upscale : false,
            format: "jpg"
        }))
        .pipe(gulp.dest(`web/img/screenshots/${size}`));
};