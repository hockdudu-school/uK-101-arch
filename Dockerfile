FROM node:10-alpine

RUN npm config set unsafe-perm true
RUN npm i -g gulp-cli
RUN apk add --no-cache graphicsmagick

WORKDIR /build/
COPY package*.json /build/
RUN npm i

COPY . /build/
RUN gulp

FROM nginx:alpine
COPY --from=0 /build/web/ /usr/share/nginx/html/