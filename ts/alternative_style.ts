const $body = $("body");
const $styleToggleButton = $(".style-toggle");
const variant_two = "variant-2";


//Set layout on page load
if (typeof Storage !== "undefined") {
    if (localStorage.alternative_layout) {
        if (localStorage.alternative_layout === "true") {
            $body.addClass(variant_two);
        } else {
            $body.removeClass(variant_two);
        }
    }
}


const toggleStyle = () => {
    $body.toggleClass(variant_two);

    if (typeof Storage !== "undefined") {
        localStorage.setItem("alternative_layout", $body.hasClass(variant_two).toString())
    }
};


$(document).keydown((e) => {
    if (e.ctrlKey && e.key == "e") {
        toggleStyle();
        e.preventDefault();
    }
});

$styleToggleButton.click(() => toggleStyle());