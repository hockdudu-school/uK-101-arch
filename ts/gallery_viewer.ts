const $imageGrid = $('.image-grid');

const $body = $('body');

const $imageGallery = () => $('.gallery-viewer');
const $galleryHolder = () => $('.gallery-holder');
const $imageGalleryButtonNext = () => $imageGallery().find('.button-next');
const $imageGalleryButtonPrev = () => $imageGallery().find('.button-prev');
const $imageGalleryButtonClose = () => $imageGallery().find('.button-close');

const $nthImage = (index: number) => $galleryHolder().children().eq(index);
const $nthImageImage = (index: number) => $nthImage(index).children('img');
const $nthImageCaption = (index: number) => $nthImage(index).children('figcaption');

let galleryImages: Array<{ image: string, low_res: string, description: string }> = [];


const initializeGallery = (figure: JQuery) => {
    if (!$body.hasClass('gallery')) {
        let $galleryViewer = $('<div/>', {class: 'gallery-viewer'});


        //Add images to array
        galleryImages = [];
        for (let fig of $imageGrid.children("figure").toArray()) {
            galleryImages.push({
                image: $(fig).children('img').data('orig').toString(),
                low_res: $(fig).children('img').attr('src').toString(),
                description: $(fig).children('figcaption').html().toString()
            });
        }

        const index = $imageGrid.children('figure').index(figure);

        const $holder = $('<div/>', {class: "gallery-holder"});

        const $nextButton = $('<div/>', {class: "button-next"});
        const $prevButton = $('<div/>', {class: "button-prev"});
        const $closeButton = $('<div/>', {class: "button-close"});

        for (let object of galleryImages) {
            $('<figure/>')
                .append($('<img/>', {src: object.low_res}) as JQuery<HTMLElement>)
                .append($('<figcaption/>', {text: object.description}) as JQuery<HTMLElement>)
                .appendTo($holder);
        }

        $galleryViewer
            .append($nextButton as JQuery<HTMLElement>)
            .append($prevButton as JQuery<HTMLElement>)
            .append($closeButton as JQuery<HTMLElement>)
            .append($holder as JQuery<HTMLElement>);

        $galleryViewer.appendTo($('main'));

        $galleryViewer.on('click', event => {
            if (event.target == event.currentTarget) closeGallery();
        });

        $imageGalleryButtonClose().on('click', () => closeGallery());

        showPicture(index);
        $body.addClass('unscrollable');
    }
};

const closeGallery = () => {
    $imageGallery().remove();
    $body.removeClass('unscrollable');
};

const showPicture = (index: number) => {

    //If low res is being shown
    if ($nthImageImage(index).attr("src").toString() != galleryImages[index].image) {
        let preload = new Image();
        $(preload).attr({src: galleryImages[index].image});

        //If on cache
        if (preload.complete) {
            $nthImageImage(index).attr("src", galleryImages[index].image);
        } else {
            $(preload).on("load", () => {
                $nthImageImage(index).attr("src", galleryImages[index].image);
            })
        }

    }
    $nthImageCaption(index).html(galleryImages[index].description);

    const nextIndex = index == galleryImages.length - 1 ? 0 : index + 1;
    const prevIndex = index == 0 ? galleryImages.length - 1 : index - 1;

    $nthImage(index).on("swipeleft", () => showPicture(nextIndex));
    $nthImage(index).on("swiperight", () => showPicture(prevIndex));

    for (let i = 0; i < galleryImages.length; i++) {
        $nthImage(i).removeClass("prev next current");
        if (i != index) $nthImage(i).off("swipeleft swiperight");

        //Direct previous || if index ==0, then if last
        if (i == index - 1 || index == 0 && i == galleryImages.length - 1) {
            $nthImage(i).addClass("prev");
        }

        //Direct next || if index is last, then if first
        else if (i == index + 1 || index == galleryImages.length - 1 && i == 0) {
            $nthImage(i).addClass("next");
        }

        else if (i == index) {
            $nthImage(i).addClass("current");
        }
    }

    $imageGalleryButtonNext().off().one('click', () => showPicture(index + 1 > galleryImages.length - 1 ? 0 : index + 1));
    $imageGalleryButtonPrev().off().one('click', () => showPicture(index - 1 < 0 ? galleryImages.length - 1 : index - 1));
};


$imageGrid.children('figure').on('click', event => {
    initializeGallery(event.currentTarget);
});