const navButton = $('#menu-button');
const navbar = $('#navbar');

navButton.click(() => {
    const isActive = navbar.hasClass("active");

    if (isActive) {
        navbar.height(navbar.innerHeight());
        navbar.animate({
            height: 0
        }, 250, () => {
            navbar.removeClass("active");
        });
    } else {
        navbar.animate({
            height: navbar[0].scrollHeight
        }, 250, () => {
            navbar.addClass("active");
            navbar.height("auto");
        });
    }
});